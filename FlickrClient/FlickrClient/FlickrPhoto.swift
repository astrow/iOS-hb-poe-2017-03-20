//
//  Photo.swift
//  FlickrClient
//
//  Created by Apple on 24/03/2017.
//  Copyright © 2017 Apple. All rights reserved.

import Foundation

enum FlickrPhotoURLCriteria: String {
    case none
    case s
    case q
    case t
    case m
    case n
    case minus = "-"
    case z
    case c
    case b
    case h
    case k
    case o
}

enum FlickrPhotoURLExtension: String {
    case jpg
    case gif
    case png
}

enum FlickrPhotoError: Error {
    case secretNotProvided
}

struct FlickrPhoto {
    
    let farm: String
    let id: String
    let isfamily: String
    let isfriend: String
    let ispublic: String
    let owner: String
    let secret: String
    let server: String
    let title: String
    let oSecret: String?
    
    
//    https://www.flickr.com/services/api/misc.urls.html
//    https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
//    or
//    https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
//    or
//    https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)
    func url(criteria: FlickrPhotoURLCriteria = .none, ext: FlickrPhotoURLExtension = .jpg) throws -> String {

        if criteria == .o && oSecret == nil {
            throw FlickrPhotoError.secretNotProvided
        }

        var url = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)"
        
        if criteria != .none {
            url += "_\(criteria.rawValue)"
        }
        
        url += ".\(ext.rawValue)"
        
        return url
    }
    
}
