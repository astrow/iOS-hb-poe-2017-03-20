//
//  FlickrGalleryCollectionViewController.swift
//  FlickrClient
//
//  Created by Thomas Gros on 02/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

private let reuseIdentifier = "FlickrGalleryCollectionViewCell"

class FlickrGalleryCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var photos = [FlickrPhoto]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        // self.collectionView!.register(FlickrGalleryCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        // Do any additional setup after loading the view.
        
        FlickrService.sharedInstance.search(with: ["tags":"landscape"]) { (photos, error) in
            guard error == nil else {
                
                // see
                // https://developer.apple.com/reference/uikit/uialertcontroller
                // http://nshipster.com/uialertcontroller/
                let alert = UIAlertController(title: "error", message: "error calling FlickrService.search \(error!)", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "ok", style: .default, handler: nil)
                alert.addAction(defaultAction)
                
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            self.photos = photos!
            
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FlickrGalleryCollectionViewCell
        
        cell.imageView.image = nil
        
        if let photoThumbnail = try? photos[indexPath.row].url(criteria: .t),
            let thumblailURL =  URL(string: photoThumbnail) {
            let downloadImageTask = URLSession.shared.dataTask(with: thumblailURL) {
                (data, response, error) in
                // TODO handle error
                if let data = data {
                    DispatchQueue.main.async {
                        cell.imageView.image = UIImage(data: data)
                    }
                } // TODO handle data empty
            }
            
            downloadImageTask.resume()
        } // TODO handle incorrect url and thumbnail
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 8, left: 4, bottom: 0, right: 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (view.bounds.size.width - 16) / 2
        let cellHeight = cellWidth * 0.8 // multiply by some factor to make cell rectangular not square
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 1, height: 1)
//        // return CGSize(width: collectionView.bounds.size.width / 2, height: collectionView.bounds.size.width / 2)
//    }
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
}
