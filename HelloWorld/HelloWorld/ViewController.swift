//
//  ViewController.swift
//  HelloWorld
//
//  Created by Apple on 20/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var helloLbl: UILabel!
    
    @IBOutlet weak var helloTxt: UITextField!
    
    @IBOutlet weak var helloBtn: UIButton!

    // target -> action
    @IBAction func onHelloBtnTapped(_ sender: Any) {
        helloTxt.resignFirstResponder()
        
//        helloLbl.text = "hello " + helloTxt.text!
        helloLbl.text = "hello \(helloTxt.text!)"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

