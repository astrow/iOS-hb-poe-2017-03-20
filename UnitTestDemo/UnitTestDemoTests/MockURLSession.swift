//
//  MockURLSession.swift
//  UnitTestDemo
//
//  Created by Apple on 04/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

class MockDataTask: URLSessionDataTask {
    
    let completionHandler: () -> Void
    
    init(completionHandler: @escaping () -> Void) {
        self.completionHandler = completionHandler
    }
    
    override func resume() {
        completionHandler()
    }
}

class MockURLSession: URLSession {
    let data: Data?
    let urlResponse: URLResponse?
    let error: Error?
    
    init(data:Data?, urlResponse: URLResponse?, error: Error?) {
        self.data = data
        self.urlResponse = urlResponse
        self.error = error
    }
    
    override func dataTask(
        with url: URL,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        
        
        let mockedCompletionHandler = {
            completionHandler(self.data, self.urlResponse, self.error)
        }
        
        
        return MockDataTask(completionHandler: mockedCompletionHandler)
    }
}
