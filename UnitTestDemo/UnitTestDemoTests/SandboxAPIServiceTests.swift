//
//  SandboxAPIServiceTests.swift
//  UnitTestDemo
//
//  Created by Apple on 04/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import XCTest
@testable import UnitTestDemo

class SandboxAPIServiceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // TODO tester que SandBoxAPIService.sharedInstance retourne bien un singleton
    
    func testHello() {

        // Arrange
        let data = "Hello world".data(using: .utf8)
        let urlResponse = HTTPURLResponse(url: URL(string: "http://fakeAPIURL")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let error: Error? = nil
        
        let mockURLSession = MockURLSession(data: data, urlResponse: urlResponse, error: error)
        
        
        let sandboxApiService = SandboxAPIService(urlSession: mockURLSession)
        
        let expect = expectation(description: "completionHandler has been called")
        
        // Act
        sandboxApiService.hello {
            result, error in
            
            // Assert
            XCTAssertNil(error)
            XCTAssertNotNil(result)
            XCTAssertEqual(result, "Hello world")
            
            // marquer l'appel du callback comme true
            expect.fulfill()
        }
        
        self.waitForExpectations(timeout: 1) {
            error in
            // handle errors
        }
        
    }
    
    
    func testHelloIfAPIReturns500() {
        
        // Arrange
        let data: Data? = nil
        let urlResponse = HTTPURLResponse(url: URL(string: "http://fakeAPIURL")!, statusCode: 500, httpVersion: nil, headerFields: nil)
        let error: Error? = nil
        
        let mockURLSession = MockURLSession(data: data, urlResponse: urlResponse, error: error)
        
        
        let sandboxApiService = SandboxAPIService(urlSession: mockURLSession)
   
        let expect = expectation(description: "completionHandler has been called")
        
        // Act
        sandboxApiService.hello { // TODO PROVOQUER UNE ERREUR 500
            result, error in
            
            // Assert
            XCTAssertNil(result)
            XCTAssertNotNil(error)
            
            //TODO  TESTER que error est du type SandboxAPIService.internalError(500)
            
            
            // marquer l'appel du callback comme true
            expect.fulfill()
        }
        
        self.waitForExpectations(timeout: 1) {
            error in
            // handle errors
        }
        
    }
    
//    func testHelloIfNetworkError() {
//        
//        // Arrange
//        let myCustomURLSession = // comment creer une URLSession qui retourne une network error
//        let sandboxApiService = SandboxAPIService(myCustomURLSession)
//        
//        
//        let expect = expectation(description: "completionHandler has been called")
//        
//        // Act
//        sandboxApiService.hello { // TODO PROVOQUER UNE NETWORK ERROR
//            result, error in
//            
//            // Assert
//            XCTAssertNil(result)
//            XCTAssertNotNil(error)
//            
//            //TODO  TESTER que error est du type SandboxAPIService.networklError()
//            
//            
//            // marquer l'appel du callback comme true
//            expect.fulfill()
//        }
//        
//        self.waitForExpectations(timeout: 1) {
//            error in
//            // handle errors
//        }
//        
//        
//    }
    
    
    
    //    func testGetUserByValidUsername() {
    //
    //        let sandboxApiService = SandBoxAPIService.sharedInstance
    //
    //        sandboxApiService.getUser(by username:String) {
    //            result, error in
    //                // assert error nil
    //                // assert result correspond au username attendu
    //        }
    //
    //    }
    
    // TODO tester /GET /users/invalidusername
    
    
    
}

