//
//  CalculatorTest.swift
//  UnitTestDemo
//
//  Created by Apple on 03/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import XCTest
@testable import UnitTestDemo

class CalculatorTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAdd5Plus3Gives8() {
        // arrange
        let calculator = Calculator()
        
        let a = 5
        let b = 3
        let expectedResult = 8
        
        // act
        let result = try! calculator.add(a: a, b: b)
       
        // assert
        XCTAssertEqual(result, expectedResult)
        XCTAssertTrue(result == expectedResult)
    }
    
    func testAddNumbersWithSumGreaterThanMaxIntThrowsCalculatorErrorIntegerOverflow() {
        let calculator = Calculator()
        
        XCTAssertThrowsError(try calculator.add(a: Int.max, b: 1), "should have thrown an CalculatorError.integerOverflow") {
            (error) in
                XCTAssertEqual(error as? CalculatorError, CalculatorError.integerOverflow)
        }
    }
    
    func testAddNumbersWithSumLowerThanMinIntThrowsCalculatorErrorIntegerOverflow() {
        let calculator = Calculator()
        
        XCTAssertThrowsError(try calculator.add(a: Int.min, b: -1), "should have thrown an CalculatorError.integerOverflow") {
            (error) in
            XCTAssertEqual(error as? CalculatorError, CalculatorError.integerOverflow)
        }
    }
    
    
    
    
    
    
    
    
}
