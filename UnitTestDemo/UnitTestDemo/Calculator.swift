//
//  Calculator.swift
//  UnitTestDemo
//
//  Created by Apple on 03/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

class Calculator {
    
    /*
     Additionne deux nombres
     Jette une CalculatorError.intergerOverflow si a + b > Int.max
    */
    func add(a: Int, b: Int) throws -> Int {
        let (result, overflow) = Int.addWithOverflow(a, b)
        
        guard overflow == false else {
            throw CalculatorError.integerOverflow
        }
        
        return result
    }
}
