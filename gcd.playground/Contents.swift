//: Playground - noun: a place where people can play
import PlaygroundSupport
import UIKit

let queue1 = DispatchQueue(label: "com.maboite.monappli.maqueue.1", qos: .background, attributes: .concurrent)
let queue2 = DispatchQueue(label: "com.maboite.monappli.maqueue.2", qos: .utility)

queue1.async {
    for i in 0..<100 {
        print("1-\(i)")
    }
    
    print("1-done")
}

queue1.async {
    for i in 0..<100 {
        print("1-1-\(i)")
    }
    
    print("1-1-done")
}

queue2.async {
    for i in 0..<100 {
        print("2-\(i)")
    }
    
    DispatchQueue.main.async {
        print("2-done")
    }
    print("tata")
}


PlaygroundPage.current.needsIndefiniteExecution = true
